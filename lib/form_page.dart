import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  const FormPage({super.key});

  @override
  State<FormPage> createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {

  final _formKey=GlobalKey<FormState>();
  Map<String,dynamic> data=<String,dynamic>{};

  final dropList=[
    'USA',
    'UAE',
    'India',
    'Russia'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Form(
        key: _formKey,
        child: ListView(
          children: [
            TextFormField(
              decoration: InputDecoration(
                labelText: ("First Name"),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(
                    color: Colors.yellow,
                    width: 1
                  )
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(
                    color: Colors.blue,
                    width: 1
                  )
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(
                    color: Colors.red,
                    width: 1
                  )
                ),

              ),
              validator: (val){
                if(val!.isEmpty){
                  return "Enter your firest name";
                }
                return null;
              },
              onSaved: (newValue) {
                data['firstName']=newValue;
              },
            ),
            const SizedBox(height: 10,),
            TextFormField(
              decoration: const InputDecoration(
                labelText: ("Last Name"),

              ),
              validator: (val){
                if(val!.isEmpty){
                  return "Enter your last name";
                }
                return null;
              },
              onSaved: (newValue) {
                data['lastName']=newValue;
              },
              
            ),
            const SizedBox(height: 10,),
            TextFormField(
              decoration: const InputDecoration(
                labelText: ("Oranizationa Name"),

              ),
              onSaved: (newValue) {
                data['org']=newValue;
              },
            ),
          const SizedBox(height: 20,),
          DropdownButton(
            value: 'India',
            items: dropList.map((e){
              return DropdownMenuItem(value: e,child: Text(e,style:const TextStyle(color: Colors.blue),),);
            }).toList(), 
            onChanged:(val){
              data['country']=val;
            }),

          ElevatedButton(
            onPressed: (){
              if(_formKey.currentState!.validate()){
                  _formKey.currentState!.save();
                print(data);
              }
            },
            child: const Text("Submit"),
          )
          ],
        )
      ),
    );
  }
}